# Salsa App API proxy

NGINX proxy app the main salsa app

## Usage

### Environemtn Variables

* 'LISTEN_PORT' - Port to listen on (default: '8000')
* 'APP_HOST' - Hostname of the app to forward requests to (default: 'app')
* 'APP_PORT' - Port of the app to forward request to (default: '9000')